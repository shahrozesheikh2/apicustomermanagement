import { AutoIncrement,  BelongsTo,  Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { customer } from "./customer";

export interface orderI {
    id : number;
    fdServiceProviderId : number;
    customerId : number;
    bookingTypeId?: number;
    subBookingTypeId : number;
    productId:number;
    customTag:string;
    statusId:number;
    partialFilled:boolean
    fixFirstId: string;
    createdAt : Date;
    updatedAt : Date;
    deletedAt : Date;
    createdBy : number;
    updatedBy : number;
    deletedBy : number;
}

@Table({
    modelName: 'order',
    tableName: 'order',
    timestamps: true
})

export class order extends Model<orderI>{

    @BelongsTo((): typeof  customer => customer )
    public customer : typeof customer ;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.INTEGER)
    public fdServiceProviderId: number;

    @ForeignKey((): typeof customer  => customer )
    @Column(DataType.INTEGER)
    public customerId: number;

  
    @Column(DataType.INTEGER)
    public bookingTypeId: number;

   
    @Column(DataType.INTEGER)
    public subBookingTypeId: number;

   
    @Column(DataType.INTEGER)
    public productId: number;

    @Column(DataType.TINYINT)
    public partialFilled:boolean

    @Column(DataType.TEXT)
    public customTag:string

    @Column(DataType.TEXT)
    public fixFirstId: string

    @Column(DataType.INTEGER)
    public statusId: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

}