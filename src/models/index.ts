import { ANY } from '../shared/common';
import { customer } from './customer';
import { languages } from './languages';
import { city } from './city';
import { country } from './country';
import { order } from './order';



export * from './customer';
export * from './customer';
export * from './city';
export * from './country';
export * from './order';





type ModelType = ANY;

export const models: ModelType = [customer,country,city,languages,order]