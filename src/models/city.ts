import { AutoIncrement, Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { customer } from ".";

export interface cityI {
    id?: number;
    countryId?: number;
    name?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deltedBy?: number;
}

@Table({
    modelName: 'city',
    tableName: 'city',
    timestamps: true
})

export class city extends Model<cityI>{

    @HasMany((): typeof customer => customer)
    public customers: typeof customer;
    
    
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.BIGINT)
    public countryId: number;

    @Column(DataType.TEXT)
    public name: string;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;
}