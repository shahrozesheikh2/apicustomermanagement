
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;

export const deleteCustomerSchema =  Joi.object({
  userId: Joi.number().integer(),
  id: Joi.number().integer().required()
})
