import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { customer, order } from '../../models/index';
import schema, { deleteCustomerSchema } from './schema';
import * as typings from '../../shared/common';


const deleteCustomer:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const customerData :typings.ANY   = await deleteCustomerSchema.validateAsync(event.body)

    const {
      id,
      // userId
    } = customerData


    var lang = event.headers['accept-language'];
    var message = 'Order Exist Against This Customer'
    if(lang =='de')
    {
       message = 'Auftrag für diesen Kunden vorhanden'
    }


    const Exists = await customer.findByPk(id);
    if(!Exists) throw new Error("Ungültige Kunden-ID");
    // Invalid Customer Id

    const orderExist = await order.findOne({where:{customerId:id, deletedAt:null}})

    if (orderExist){
      return formatJSONResponse({
        statusCode: 403,
        message
        // message: `Order Exist Against This Customer`
       });
    }

     const data = await customer.update({
          deletedAt:new Date
        }, { 
            where: {
                id: id,
            }
        }); 
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message: `Erfolgreich gelöscht`,
      // message: `Successfully deleted`,

      data
     });
     
   } catch(error) {
     console.error(error);
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(deleteCustomer);