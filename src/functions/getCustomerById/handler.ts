import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { city, country, customer } from '../../models/index';
import schema, { getCustomerSchema } from './schema';
import * as typings from '../../shared/common';
import { languages } from 'src/models/languages';


const getCustomerById:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    const customerData :typings.ANY   = await getCustomerSchema.validateAsync(event.body)

    let  data = await customer.findOne( {
            where: {
                id: customerData.id,
            },
            include:[{
              as: 'country',
              model: country,
              attributes:['id','name']
            },
            {
              as: 'city',
              model: city,
              attributes:['id','name']
            },
            {
              as: 'languages',
              model: languages,
              attributes:['id','name']
              
            }
          ]
        });
        if(!data){
          return formatJSONResponse({
            statusCode: 200,
            //  message: `Erfolgreich erhalten`,
             message: `Data Not Exist Against This Id`
           });
        }
        
        let visitAddressCountry: country, visitAddressCity: city , billingAddressCountry: country, billingAddressCity: city
        console.log("data",data)

        if(data.visitAddressCountryId && data.visitAddressCityId){
          visitAddressCountry = await country.findOne({attributes:['id','name'],where:{id: data.visitAddressCountryId}})
    
           visitAddressCity =  await city.findOne({attributes:['id','name'], where: {id: data.visitAddressCityId} })
        

        }  
        if(data.billingAddressCountryId && data.billingAddressCityId){
           billingAddressCountry = await country.findOne({attributes:['id','name'],where:{id: data.billingAddressCountryId}})
        
           billingAddressCity = await city.findOne({attributes:['id','name'], where: {id: data.billingAddressCityId} })
        
        }

        data = JSON.parse(JSON.stringify(data))

        const response = {...data, visitAddressCountry, visitAddressCity, billingAddressCountry, billingAddressCity}

      if(!data){
        // throw Error("Customer Not Exists Again Current Id")
        throw Error("Kunde existiert nicht mehr Aktuelle ID")
      }
    await sequelize.connectionManager.close();

      return formatJSONResponse({
      statusCode: 200,
       message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,
       response
     });
     
   } catch(error) {
     console.error(error);
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getCustomerById);