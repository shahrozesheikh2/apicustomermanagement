
import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;

export const updateCustomerSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  id: Joi.number().integer().required(),
  customerDetails:Joi.object({
    companyId:Joi.number().integer(),
    email:Joi.string().email(),
    customerType:Joi.number().integer().valid(0,1),
    companyName:Joi.when('customerType',{is: 1, then: Joi.string().required()}),
    firstName:Joi.string(),
    lastName:Joi.string(),
    language:Joi.array(),
    languageId: Joi.number().integer(),
    notes: Joi.string(),
    mobileNumberExtension: Joi.string(),
    mobileNumber: Joi.string(),
    isWhatsAppMblNo: Joi.boolean(),
    additionNumberExtension:Joi.string().allow(null).allow(''),
    additionNumber:Joi.string().allow(null).allow(''), 
    isWhatsAppAddNo: Joi.boolean(),
    taxId: Joi.string().allow(null).allow(''),
    customerAddress:Joi.string(),
    customerNumber:Joi.string(),
    customerAddition:Joi.string().allow(null).allow(''),
    customerNotes:Joi.string().allow(null).allow(''),
    customerCountryId:Joi.number().integer(),
    customerCityId:Joi.number().integer(),
    customerPostalCode:Joi.number().integer(),
    isCustomerElevator:Joi.boolean(),
    isVisitAddressSame:Joi.boolean(),
    visitAddress:Joi.when('isVisitAddressSame',{is: false, then: Joi.string()}),
    visitAddressNumber:Joi.when('isVisitAddressSame',{is: false, then: Joi.string()}),
    visitAddressAdditionInfo:Joi.when('isVisitAddressSame',{is: false, then: Joi.string()}),
    visitAddressNotes:Joi.when('isVisitAddressSame',{is: false, then: Joi.string()}),
    visitAddressCityId:Joi.when('isVisitAddressSame',{is: false, then: Joi.number().integer()}),
    visitAddressCountryId:Joi.when('isVisitAddressSame',{is: false, then: Joi.number().integer()}),
    visitAddressPostalCode:Joi.when('isVisitAddressSame',{is: false, then: Joi.number().integer()}),
    isVisitAddressElevator:Joi.when('isVisitAddressSame',{is: false, then: Joi.boolean()}),
    isBillingAddressSame:Joi.boolean(),
    billingAddress:  Joi.when('isBillingAddressSame',{is: false, then: Joi.string()}),
    billingAddressNumber:  Joi.when('isBillingAddressSame',{is: false, then: Joi.string()}),
    billingAddressAdditionInfo:  Joi.when('isBillingAddressSame',{is: false, then: Joi.string()}),
    billingAddressNotes: Joi.when('isBillingAddressSame',{is: false, then: Joi.string()}),
    billingAddressCityId:Joi.when('isBillingAddressSame',{is: false, then: Joi.number().integer()}),
    billingAddressCountryId: Joi.when('isBillingAddressSame',{is: false, then: Joi.number().integer()}),
    billingAddressPostalCode: Joi.when('isBillingAddressSame',{is: false, then: Joi.number().integer()}),
    isbillingAddressElevator:Joi.when('isBillingAddressSame',{is: false, then: Joi.boolean()}), 
    customTags:Joi.array()
  }).required()
})
