import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { customer, customerI } from '../../models/index';
import schema, { updateCustomerSchema } from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';



const updateCustomerDetails:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    var lang = event.headers['accept-language'];
    var message = 'Updated Successfully'
    if(lang =='de')
    {
       message = 'Erfolgreich geupdated'
    }

    const customerData :typings.ANY   = await updateCustomerSchema.validateAsync(event.body)

    const {
      id,
      userId,
      customerDetails
    } = customerData
    

    

    const Exists = await customer.findByPk(id);

    if(!Exists) throw new Error("Ungültige Kunden-ID");
    // Invalid Customer Id
    // const existEmail = await customer.findOne({
    //   where:{
    //     email:customerDetails.email,
    //     companyId:{[Op.eq]:customerDetails.companyId},
    //     id:{[Op.ne]:id}
    //   }})

    // if(existEmail){
    //   throw Error("E-Mail ist bereits vorhanden")
    //   // Email Already Exist
    // } 

    if(customerDetails.customTags){

      let allCustomers = await customer.findAll({where:{companyId:customerDetails.companyId,id:{[Op.ne]:id}}})//.map(c => c.customTags)
    
      allCustomers = JSON.parse(JSON.stringify(allCustomers))

      // console.log("allusers",JSON.parse(JSON.stringify(allCustomers)))

      let tagCheck = false

      if(allCustomers.length != 0){
        for(let u of allCustomers){
          console.log("uuu",u)
          if(u.customTags!=null){
          for(let t of u.customTags){
            for(let tag of customerDetails.customTags ){
              if (t == tag){
                tagCheck = true
                console.log("count")
                break;
              }
            }
          }}    
        }
        console.log("tagCheck",tagCheck);
      }
   
      if(tagCheck){
        return formatJSONResponse({
          statusCode: 403, 
          message: "Customs Tags Already Exist It Must Be Unique"
        });
      }

    }

    const customerObj = { ...customerDetails, updatedBy:userId}

    console.log("customerObj",customerObj)

     await customer.update({
          ...customerObj
        }, { 
            where: {
                id: id,
            }
      }); 

      const response : customerI = await customer.findOne({
          where: {
              id: customerData.id,
          }
      });
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message,
      //  message: `Erfolgreich aktualisiert`,
      //  Successfully Updated
       response
     });
     
   } catch(error) {
     console.error(error);
    await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(updateCustomerDetails);