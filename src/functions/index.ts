export { default as hello } from './hello';
export { default as createCustomer } from './createCustomer';
export { default as updateCustomerDetails } from './updateCustomerDetails';
export { default as getCustomerById  } from './getCustomerById';
export { default as getAllCustomer  } from './getAllCustomer';
export { default as deleteCustomer  } from './deleteCustomer';



