import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { customer } from '../../models/index';
import schema, { customerSchema }  from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';

const createCustomer:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{

    try{

      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }

    sequelize.authenticate()

    var lang = event.headers['accept-language'];
    var message = 'Customer Created Successfully'
    if(lang =='de')
    {
       message = 'Kundenprofil erfolgreich erstellt'
    }

    const customerData :typings.ANY = await customerSchema.validateAsync(event.body)

    const {
      userId,
      customerDetails
    } = customerData
    console.log("customerDetails",customerDetails)
    let response = await customer.findOne({where:{email:customerDetails.email,companyId:customerDetails.companyId,
      // customTags:{[Op.in]:customerDetails.customTags}
    }})

   
    // console.log("reponse",reponse);
    // throw Error
  if(!response){

    let allCustomers = await customer.findAll({where:{companyId:customerDetails.companyId}})//.map(c => c.customTags)
    
    allCustomers = JSON.parse(JSON.stringify(allCustomers))
    // console.log("allusers",JSON.parse(JSON.stringify(allCustomers)))

    let tagCheck = false

    if(allCustomers.length != 0 && customerDetails.customTags){
      for(let u of allCustomers){
        // console.log("uuu",u)
        if (u.customTags != null){
        for(let t of u.customTags){
          for(let tag of customerDetails.customTags ){
            if (t == tag){
              tagCheck = true
              console.log("count")
              break;
            }
          }
        }}    
      }
      console.log("tagCheck",tagCheck);
    }
   
    if(tagCheck){
      return formatJSONResponse({
        statusCode: 403, 
        message: "Customs Tags Already Exist It Must Be Unique"
      });
    }
      
    const number1  = parseInt(`${Math.random() * 1000}`)

    const number2  = parseInt(`${Math.random() * 1000}`)


    const fixFirstId = `FF-CC-${number1}-${number2}`

    console.log("fixFirstId",fixFirstId)

    
    const customerObj = { ...customerDetails, createdBy:userId,fixFirstId}

    response = await customer.create(customerObj)

  }
    await sequelize.connectionManager.close();

     return formatJSONResponse({
       statusCode: 200,
       message,
      //  message: `Kundenprofil erfolgreich erstellt`,
      //  message: `Customer Created Successfully`,
      response
     });
     
   } catch(error) {
     console.error(error);
    //  await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403, 
      message: error.message
    });
     
   }
 }
 export const main = middyfy(createCustomer);