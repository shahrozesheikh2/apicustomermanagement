import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  },
} as const;

export const customerSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  customerDetails:Joi.object({
      companyId:Joi.number().integer().required(),
      customerType:Joi.number().integer().valid(0,1).required(),
      companyName:Joi.when('customerType',{is: 1, then: Joi.string().required()}),
      firstName:Joi.string().required(),
      lastName:Joi.string().required(),
      email:Joi.string().email({ tlds: { allow: ['com', 'net' , 'io'] } }).required() ,
      language:Joi.array(),
      languageId: Joi.number().integer(),
      notes: Joi.string(),
      mobileNumberExtension:Joi.string().required(),
      mobileNumber: Joi.string().required(),
      isWhatsAppMblNo: Joi.boolean().required(),
      additionNumberExtension:Joi.string().allow(null).allow(''),
      additionNumber:Joi.string().allow(null).allow(''),  
      isWhatsAppAddNo: Joi.boolean(),
      taxId: Joi.string().required(),
      customerAddress:Joi.string().required(),
      customerNumber:Joi.string().required(),
      customerAddition:Joi.string().allow(null).allow(''),
      customerNotes:Joi.string().allow(null).allow(''),
      customerCountryId:Joi.number().integer().required(),
      customerCityId:Joi.number().integer().required(),
      customerPostalCode:Joi.number().integer().required(),
      
      // .messages({
      //   'number.base': `It must be a number`,
      //   'any.empty': `cannot be an empty field`,
      //   'any.required': `is a required field`
      // }),
      isCustomerElevator:Joi.boolean().required(),
      isVisitAddressSame:Joi.boolean().required(),
      visitAddress:Joi.when('isVisitAddressSame',{is: false, then: Joi.string().required()}),
      visitAddressNumber:Joi.when('isVisitAddressSame',{is: false, then: Joi.string().required()}),
      visitAddressAdditionInfo:Joi.when('isVisitAddressSame',{is: false, then: Joi.string().allow(null).allow('')}),
      visitAddressNotes:Joi.when('isVisitAddressSame',{is: false, then: Joi.string().allow(null).allow('')}),
      visitAddressCityId:Joi.when('isVisitAddressSame',{is: false, then: Joi.number().integer().required()}),
      visitAddressCountryId:Joi.when('isVisitAddressSame',{is: false, then: Joi.number().integer().required()}),
      visitAddressPostalCode:Joi.when('isVisitAddressSame',{is: false, then: Joi.number().integer().required()}),
      isVisitAddressElevator:Joi.when('isVisitAddressSame',{is: false, then: Joi.boolean().required()}),
      isBillingAddressSame:Joi.boolean().required(),
      billingAddress:  Joi.when('isBillingAddressSame',{is: false, then: Joi.string().required()}),
      billingAddressNumber:  Joi.when('isBillingAddressSame',{is: false, then: Joi.string().required()}),
      billingAddressAdditionInfo:  Joi.when('isBillingAddressSame',{is: false, then: Joi.string().allow(null).allow('')}),
      billingAddressNotes: Joi.when('isBillingAddressSame',{is: false, then: Joi.string().allow(null).allow('')}),
      billingAddressCityId:Joi.when('isBillingAddressSame',{is: false, then: Joi.number().integer().required()}),
      billingAddressCountryId: Joi.when('isBillingAddressSame',{is: false, then: Joi.number().integer().required()}),
      billingAddressPostalCode: Joi.when('isBillingAddressSame',{is: false, then: Joi.number().integer().required()}),
      isbillingAddressElevator:Joi.when('isBillingAddressSame',{is: false, then: Joi.boolean().required()}), 
      customTags:Joi.array()
  }).required()
})

