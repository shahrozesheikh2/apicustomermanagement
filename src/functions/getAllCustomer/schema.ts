import * as Joi from "joi";

export default {
  type: "object",
  properties: {
  }
} as const;


export const getAllCustomerSchema =  Joi.object({
  userId: Joi.number().integer().required(),
  companyId: Joi.number().integer().required(),
  offset: Joi.number().min(0).default(0),
  limit: Joi.number().max(999).default(10),
  filters:Joi.object({
    firstName:Joi.string().optional().allow(null).allow(''),
    lastName:Joi.string().allow(null).allow(''),
    fixfirstId:Joi.string().allow(null).allow(''),
    email:Joi.string().allow(null).allow(''),
    fixFirstId:Joi.string().allow(null).allow(''),
    currentState:Joi.number().integer().allow(null).allow(''),
    customerType:Joi.number().integer().allow(null).allow('')
  })
  
})
