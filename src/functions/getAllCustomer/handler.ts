import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { sequelize } from 'src/config/database';
import { customer, order } from '../../models/index';
import schema, { getAllCustomerSchema } from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';

const getAllCustomer:ValidatedEventAPIGatewayProxyEvent<typeof schema> = async(event)=>{
    try{
      sequelize.connectionManager.initPools();

      // restore `getConnection()` if it has been overwritten by `close()`
      if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
        delete sequelize.connectionManager.getConnection;
      }
    sequelize.authenticate()

    var lang = event.headers['accept-language'];
    var message = 'Successfully Recieved'
    if(lang =='de')
    {
       message = 'Erfolgreich erhalten'
    }

    const data :typings.ANY   = await getAllCustomerSchema.validateAsync(event.body)

    const{
      // userId,
      limit,
      offset,
      companyId,
      filters
    } = data

    let  whereClause: { [key: string]: typings.ANY }  = {}

    if (filters?.firstName && filters?.firstName.length) {
      whereClause.firstName = {  [Op.startsWith]: filters.firstName };
    }

    if (filters?.lastName && filters?.lastName.length) {
      whereClause.lastName = {  [Op.startsWith]: filters.lastName };
    }

    if (filters?.email && filters?.email.length) {
      whereClause.email = {  [Op.startsWith]: filters.email };
    }

    if (filters?.fixFirstId && filters?.fixFirstId.length) {
      whereClause.fixFirstId = {  [Op.startsWith]: filters.fixFirstId };
    }

    if (filters?.currentState) {
      whereClause.currentState =  filters.currentState;
    }

    if (filters?.customerType) {
      whereClause.customerType =  filters.customerType;
    }

    
    console.log("filters2",whereClause)
    let customers : typings.ANY = await customer.findAndCountAll(
    { 
      where:{
        companyId,
        // createdBy:userId,
        deletedAt:null,
        ...whereClause
      },
      include:[{
        as:'order',
        model:order,
        // attributes: [[Sequelize.fn('Count',Sequelize.col('customerId')),'noOfOrders']],
      }],
      limit:limit,
      offset: offset * limit})

      customers  = JSON.parse(JSON.stringify(customers))

      console.log("customers",customers)

      // let custData = []; 
      // for (let arr of data1){
      //   const noOfOrders = arr.order?.length
      //   console.log("noOfOrders",noOfOrders)
      //    arr = {
      //     ...arr,
      //     noOfOrders
      //   }
      //   custData.push(arr)
      // }
      let response = customers.rows.map(c =>({
        ...c,
        noOfOrders:c.order?.length,
      }))
      
      const cons = await customer.count({
        where:{
          companyId,
          deletedAt:null,
          // createdBy:userId,
          ...whereClause
        }
      })
      console.log("con",cons)
      response = {
        count:cons ,//customers.count,
        rows:response
      }
  
      if(!response){
        throw Error("Kunde existiert nicht mehr Aktuelle ID")
        // throw Error("Customer Not Exists Again Current Id")

      }
      await sequelize.connectionManager.close();

     return formatJSONResponse({
      statusCode: 200,
      message,
      // message: `Erfolgreich erhalten`,
      //  message: `Successfully Recieved`,
       response
     });
     
   } catch(error) {
     console.error(error);
    await sequelize.connectionManager.close();
     return formatJSONResponse({
      statusCode: 403,
      message: error.message
    });
     
   }
 }
 export const main = middyfy(getAllCustomer);